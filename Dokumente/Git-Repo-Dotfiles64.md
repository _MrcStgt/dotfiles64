# Load config on new computer

1. Edit .zshrc and/or .bashrc to set the config-alias to git-command w/ git-dir and work-tree=Home directory:  
`alias config='/usr/bin/git --git-dir=$HOME/.dotfiles64/ --work-tree=$HOME`

2. Ignore .dotfiles64-directory:  
`cd ~ && echo ".dotfiles64" » .gitignore`

3. Clone dotfiles64 into the .dotfiles64-folder:  
`git clone --bare <YOUR-GIT-REPO> $HOME/.dotfiles`

4. Checkout contents from .cfg to your home directory:  
`config checkout`  
It may happen, that git wants to overwrite your existing dotfiles (.vimrc, .bashrc, etc.). If this is the case, you may use the following command to move all offending files into a backup-folder:  
`cd ~ && mkdir -p .config-backup && config checkout 2>&1 | egrep "\s+\." | awk {'print $1'} | xargs -I{} mv {} .config-backup/{}`

5. Don't show untracked files as you want only add manually set dotfiles explicitly under git-control:  
`config config --local status.showUntrackedFiles no`
`'`

# configured software packages

* compton/picom
* bgchd 
* scrot
* xfce4-terminal
* conky
* polybar
* dunst
* i3-gaps
* rofi
* urxvtc
