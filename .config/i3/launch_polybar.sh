#!/bin/bash
{
	killall -q polybar
	
	echo "---restart---" | tee -a /tmp/polybar_top.log /tmp/polybar_bottom.log

	outputs=$(xrandr --query | grep " connected" | cut -d" " -f1)
	tray_output=VNC-0

	for m in $outputs; do
		if [[ $m == "DVI-D-0" ]]; then
			tray_output=$m
		fi
	done

	for m in $outputs; do
		export MONITOR=$m
		export TRAY_POSITION=none
		if [[ $m == $tray_output ]]; then
			TRAY_POSITION=right
		fi

		polybar --reload top >>/tmp/polybar_top.log 2>&1 &
		polybar --reload bottom >>/tmp/polybar_bottom.log 2>&1 &
	done
}
