I have a launch_polybar.sh script which has a section like this:

if type "xrandr"; then
  for m in $(xrandr --query | grep " connected" | cut -d" " -f1); do
    MONITOR=$m polybar --reload example &
  done
else
  polybar --reload example &
fi
And then in my polybar config, I have:

[bar/example]
monitor = ${env:MONITOR:}
[..]
Works on every machine I've ever used it on, whether it was a multi-monitor setup or not :)
