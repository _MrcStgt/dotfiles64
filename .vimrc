runtime! archlinux.vim

if !has('nvim')
  set ttymouse=xterm2
endif

syntax on
filetype plugin on
set number
set ruler
set tabstop=2
set shiftwidth=2
set encoding=utf-8
set laststatus=2
set clipboard=unnamedplus
set ignorecase
set smartcase

" via Escape in den NORMAL-Mode zurück
tnoremap <Esc> <C-\><C-n>       

" 256 colors
set t_Co=256

" NERDTree via F6 ein- und ausschalten
map <F6> :NERDTreeToggle<CR>

" Plugins
set nocompatible
filetype off

" runtimepath für Vundle und Initialisierung
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" Vundle verwaltet sich selbst: 
Plugin 'VundleVim/Vundle.vim'
Plugin 'jiangmiao/auto-pairs'
Plugin 'scrooloose/nerdtree'
Plugin 'davidhalter/jedi-vim'
Plugin 'jistr/vim-nerdtree-tabs'
Plugin 'vim-syntastic/syntastic'
Plugin 'ervandew/supertab'
Plugin 'mboughaba/i3config.vim'
Plugin 'morhetz/gruvbox'
Plugin 'vim-scripts/twilight256.vim'
Plugin 'itchyny/lightline.vim'
Plugin 'MarcWeber/vim-addon-mw-utils'
Plugin 'tomtom/tlib_vim'
Plugin 'garbas/vim-snipmate'
Bundle 'gabrielelana/vim-markdown'
Plugin 'w0rp/ale'
Plugin 'honza/vim-snippets'
Plugin 'ryanoasis/vim-webdevicons'
Plugin 'ryanoasis/vim-devicons'

" Alle Plugins müssen vor dieser Zeile stehen! 
call vundle#end()
filetype plugin indent on

colorscheme twilight256

" Plugins-Konfiguration 

"devicons
let g:webdevicons_enable = 1
let g:webdevicons_enable_nerdtree = 1
let g:webdevicons_enable_airline_statusline = 1
let g:webdevicons_enable_ctrlp = 1

"syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

"jedi-vim
let g:jedi#completions_command = "<C-Tab>"

set noshowmode
set guifont=Source\ Code\ Pro\ 14

aug i3config_ft_detection
	au!
	au BufNewFile,BufRead ~/.config/i3/config set filetype=i3config
aug end
